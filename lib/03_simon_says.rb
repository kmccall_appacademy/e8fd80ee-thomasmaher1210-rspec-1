def echo(call)
  call
end

def shout(call)
  words = call.split(" ")
  (words.map {|word| word.upcase}).join(' ')
end


def repeat(call, times=2)
  repeated = call
  while times > 1
    times -= 1
    repeated += " #{call}"
  end
  repeated
end

def start_of_word(call, start=0)
  if start > 0
    letters = call.split("")[0..start-1]
  else
    return call.split("")[0]
  end
  letters.join
end

def first_word(call)
  call.split(" ")[0]
end

def titleize(call)
  arr = []
  call.split(" ").each_with_index do |word, idx|
    if idx == 0
      p idx
      arr << word[0].upcase + word[1..-1]
    elsif (word == 'the') || (word == 'and') || (word == 'over')
      arr << word
    else
      arr << word[0].upcase + word[1..-1]
    end
  end
  arr.join(" ")
end
