def add(n1, n2)
  n1 + n2
end

def subtract(n1, n2)
  n1 - n2
end

def sum(arr)
  arr.push(0).reduce(:+)
end

def multiply(n)
  if n != []
    return n.reduce(:*)
  else
    return 0
  end
end

def power(b, pow)
  b ** pow
end

def factorial(f)
  if f > 0
    return (1..f).reduce(:*)
  else
    0
  end
end
