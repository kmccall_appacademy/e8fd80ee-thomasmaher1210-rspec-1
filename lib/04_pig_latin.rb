def translate(phrase)
  translation = phrase.split.map {|word| "#{word_flip(word)}ay"}
  translation.join(" ")
end

def word_flip(word)
  vowels = 'aeiouAEIOU'
  current_arr = []
  idx = 0
  until idx == word.length
    if (word[idx] == 'q') && (word[idx+1] == 'u')
      idx += 2
    elsif vowels.include?(word[0])
      return word
    elsif vowels.include?(word[idx])
      current_arr << word[idx..-1] + word[0..idx-1]
      return current_arr.join
    else
      idx += 1
    end
  end
end
